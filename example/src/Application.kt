package me.jfenn

import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.response.*
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route
import io.ktor.routing.routing
import me.jfenn.ktordocs.RestDocsFeature
import me.jfenn.ktordocs.docs
import me.jfenn.ktordocs.model.AuthenticationInfo
import me.jfenn.ktordocs.model.ParameterInfo
import me.jfenn.ktordocs.restDocumentation

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module() {
    install(Authentication) {
        basic("basicAuth") {
            realm = "Ktor Server"
            validate { credentials ->
                if (credentials.name == credentials.password)
                    UserIdPrincipal(credentials.name)
                else null
            }
        }
    }

    install(RestDocsFeature) {
        baseUrl = "https://example.com"

        title = "Example API"
        desc = """
            Basic documentation generated for **testing** and _demo_ purposes.
            
            Find the source code [here](https://code.horrific.dev/james/ktordocs)!
        """.trimIndent()

        authMethod("basicAuth") {
            type(AuthenticationInfo.Type.Basic)
            title = "Basic Authentication"
            desc = "A simple authentication method **intended for debugging**, which _only_ checks whether a username is equal to the provided password."
        }

        defaultProperties {
            param("accept") {
                desc = "The content type to return - such as `application/json`"
                location = ParameterInfo.In.Header
                type = "Content Type"
                example = "application/json"
            }
        }
    }

    routing {
        route("/api") {
            get("/hello") {
                docs {
                    title = "Hello world"
                    desc = "Responds with _\"hello world\"_ :)"
                }

                call.respondText("""{"hello": "world"}""", ContentType.Application.Json)
            }

            get("/user/{username?}") {
                docs {
                    title = "Get user info"
                    desc = "Returns an object representation of the requested user ID."
                    param("username") {
                        type = "SHA1 hash"
                        desc = "A valid ID or username of the user to fetch."
                    }
                    responds {
                        desc = "If the user has been found."
                        exampleJson(UserInfo::class)
                    }
                    responds(HttpStatusCode.NotFound) {
                        desc = "If the user doesn't exist."
                    }
                }

                call.respondText("""
                    { "id": "abc123", "name": "Someone Person", "socialLinks": [ "https://example.com/" ] }
                """.trimIndent())
            }

            authenticate("basicAuth") {
                post("/user/{username}") {
                    docs {
                        title = "Modify user info"
                        desc = "Updates the provided user info in the database."
                        param("username") {
                            type = "SHA1 hash"
                            desc = "A valid ID or username of the user to update."
                        }
                        responds {
                            desc = "If the user was successfully updated"
                        }
                    }
                }
            }
        }

        restDocumentation("/")
    }

}


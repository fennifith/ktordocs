package me.jfenn

class UserInfo(
    val id: String,
    val name: String,
    val socialLinks: List<String>
)